/******************************************************************************/
/*   (C) Copyright HTL - HOLLABRUNN  2009-2009 All rights reserved. AUSTRIA  */ 
/*                                                                           */ 
/* File Name:   Keyboard                                                     */
/* Autor: 		Andreas Sch�fberger, Mathias Pichler                           */
/* Version: 	V1.00                                                          */
/* Description: Tastatur auf UART ausgabe                */
/******************************************************************************/
/* History: 	V1.00  creation										          */
/******************************************************************************/
//#include <stm32f10x.h>
#include <ARMV10_STD.h>
#include <math.h>

/*---------------------------Prototypes ---------------------------------------*/
void uart_init(unsigned long);
void uart_put_char(char);
void uart_put_string(char *);
void uart_put_hex(char); 
void nib2asc(char *);
void init_port(void);

/*-----------------------------  Bit Banding Alias Adesses -------------------*/
#define GPIOA_IDR GPIOA_BASE + 2*sizeof(uint32_t)    // Calc peripheral address GPIOA IDR
#define BITBAND_PERI(a,b) ((PERIPH_BB_BASE + (a-PERIPH_BASE)*32 + (b*4)))  
#define Data  *((volatile unsigned long *)(BITBAND_PERI(GPIOA_IDR,2)))	// Data PA2
#define CLK  *((volatile unsigned long *)(BITBAND_PERI(GPIOA_IDR,1)))	// CLK PA1
/******************************************************************************/
/*            U N T E R P R O G R A M M:    init_port                         */
/*                                                                            */
/* Aufgabe:   Initialisiert Portleitung PA0 f�r Schalter  SW0                 */
/* Input:                                                                     */
/* return:	                                                                  */
/******************************************************************************/
void init_port(void) {
int temp;

RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;       // enable clock for GPIOA (APB2 Peripheral clock enable register)

// Schalter auf Led /Schalterplatine
temp = GPIOA->CRL;
temp &= 0xFFFFF00F;	// PA1, PA3 Konfigurationsbits l�schen
temp |= 0x00000880;	// PA1, PA3 als Input definieren
GPIOA->CRL = temp;
GPIOA->BSRR = 0x6;  // PA0 internen Pull Up aktivieren (ODR Register)
} 
/******************************************************************************/
/*                                MAIN function                               */
/******************************************************************************/
int main (void) {
uart_init(9600);
init_port();
	int bitnr=8,help;
	char wert;
do
{
	do {			 // falling edge
	   } while (CLK ==1);
	if(bitnr==8)
		 {
			bitnr--;
		}
do{
	help=0;
	do{
		  do {			 // falling edge
	   } while (CLK ==1);
			help=1;
	 }while(help==0);
		 wert = wert <<1;
	}while (1==1);
	}
